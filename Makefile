ENV ?= prod
console ?= bin/console
SYMFONY_BRANCH ?= master


opt_composer:=--dev
ifeq ("prod", "${ENV}")
nodebug:=--no-debug
opt_composer:=--no-dev --optimize-autoloader
endif

port ?= 80






SQL = $(shell echo $(DATABASE_URL) | sed 's;.*root_dir%;$(PWD)/app;g')



ifeq ("$(wildcard $(SQL))","$(SQL)")
all: INIT
else
all: COMPOSER-INSTALL DATABASE_START SERVER
endif




INIT: COMPOSER-INSTALL SERVER



DATABASE_START:
	php $(console) doctrine:database:create 
	php $(console) doctrine:schema:update --force
	php $(console) doctrine:fixtures:load -n


SERVER:
	php $(console) server:run -p $(port) 0.0.0.0 --env=$(ENV) $(nodebug) -vvv

GIT_PULL:
	git pull origin $(SYMFONY_BRANCH)

UPDATE:
	@echo "ya en funcionamiento"

CACHE-CLEAR:
	php $(console) cache:clear --env=$(ENV)

COMPOSER_SELFUPDATE:
	composer.phar self-update

COMPOSER-INSTALL: GIT_PULL COMPOSER_SELFUPDATE
	composer.phar install 

COMPOSER-UPDATE: GIT_PULL COMPOSER_SELFUPDATE
	composer.phar update
